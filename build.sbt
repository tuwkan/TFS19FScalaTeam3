name := "TFS19FScalaTeam3"

version := "0.1"

scalaVersion := "2.12.8"

val doobieVersion = "0.8.4"
val http4sVersion = "0.20.0"
val circeVersion = "0.11.1"

libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "2.2.0"
libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.4.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0"
libraryDependencies += "org.scalatest" %% "scalatest-flatspec" % "3.2.0-M2"

libraryDependencies +=  "io.monix" %% "monix" % "3.0.0"

libraryDependencies +=  "org.tpolecat" %% "doobie-core" % doobieVersion
libraryDependencies +=  "org.tpolecat" %% "doobie-h2" % doobieVersion

libraryDependencies +=  "org.http4s" %% "http4s-dsl" % http4sVersion
libraryDependencies +=  "org.http4s" %% "http4s-blaze-server" % http4sVersion
libraryDependencies +=  "org.http4s" %% "http4s-blaze-client" % http4sVersion
libraryDependencies +=  "org.http4s" %% "http4s-circe" % http4sVersion

libraryDependencies +=  "io.circe" %% "circe-generic" % circeVersion

libraryDependencies +=  "io.chrisdavenport" %% "log4cats-slf4j" % "1.0.0"
libraryDependencies +=  "ch.qos.logback" % "logback-classic" % "1.2.3"