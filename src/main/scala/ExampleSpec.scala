package Parser

import org.scalatest._

class ExampleSpec extends FlatSpec with Matchers {
  val checkLength = List(
    ("Жара", 6),
    ("Доктор Сон", 6),
    ("", 0),
    ("Доктор Стрейнджлав, или Как я научился не волноваться и полюбил атомную бомбу", 6),
    ("Аннигиляция", 2),
    ("стагнация", 0),
    ("eXistenZ", 2),
    ("Преследование и убийство Жан-Поля Марата, представленное актерской труппой госпиталя в Шарантоне под руководством маркиза де Сада", 6),
  )
  checkLength.foreach {
    case (input, expected) =>
      s"download($input)" should s"return $expected" in {
        val actual = DownLoader.downLoad(input)
        actual.length should be(expected)
      }
  }

  val checkNames = List(
    ("Жара", List("Жара","Жара","Жара","Жара","Жара","Жара")),
    ("Доктор Сон", List("Доктор Сон", "Под гипнозом","Доктор Стрэндж","Доктор Хаус (сериал)","Доктор Кто (сериал)","Доктор Стрейнджлав, или Как я научился не волноваться и полюбил атомную бомбу")),
    ("eXistenZ", List("Экзистенция", "Туда и сюда")),
  )
  checkNames.foreach {
    case (input, expected) =>
      s"download($input)" should s"return $expected" in {
        val actual = DownLoader.downLoad(input)
        actual.map(x => x.title) should be(expected)
      }
  }

  val checkLinks = List(
    ("Доктор Сон", Links("https://www.kinopoisk.ru/film/1048255/cast/#actor", "https://www.kinopoisk.ru/film/1048255/video/", "https://www.kinopoisk.ru/film/1048255/stills/", "https://www.kinopoisk.ru/film/1048255/posters/","https://www.kinopoisk.ru/film/1048255/afisha/","https://www.kinopoisk.ru/film/1048255/sites/")),
  )
  checkLinks.foreach {
    case (input, expected) =>
      s"download($input)" should s"return $expected" in {
        val actual = DownLoader.downLoad(input)
        actual.head.links should be(expected)
      }
  }

}