package fintech.team3

import Parser.DownLoader
import doobie.syntax.connectionio._
import doobie.util.transactor.Transactor
import monix.catnap.{ConcurrentQueue, Semaphore}
import monix.eval.{MVar, Task}


trait AppService {
  def init: Task[Unit]

  def find(text: String, chatId: Long): Task[Unit]

  val maxParallelReqs = 3

}

class AppServiceImpl(botService: BotService, database: Database, transcator: Transactor[Task]) extends AppService {

  override def init: Task[Unit] =
    database.createTables.transact(transcator)

  override def find(text: String, chatId: Long): Task[Unit]  = {

//    for {
//      mvar <- MVar.empty[Unit]
//      queue <- ConcurrentQueue.bounded[Task, Unit](1024)
//      semaphore <- Semaphore[Task](maxParallelReqs)
//      _ <- queue.offer(text)
//      _ <- handleQueue(CrawlerState(queue, mvar, semaphore, Nil, Set.empty, 1)).start
//      result <- mvar.read
//    } yield result

    for {
      result <- database.loadResults(text).transact(transcator)
    } yield result match {
      case v@Some(_) => botService.sendMessage(v, chatId)
      case None => botService.sendMessage(DownLoader.downLoad(text).headOption, chatId)
    }
    Task()
  }

}
