package fintech.team3

import Parser.RawData

trait BotService {
    def sendMessage(rawData: Option[RawData], chatId: Long): Unit = println(rawData.getOrElse("Empty"))
}

object BotService extends BotService