package fintech.team3

import Parser.{Links, RawData}
import doobie.ConnectionIO
import doobie.Update
import doobie.implicits._


trait Database {

  val createTables =
    for {
      _ <- sql"""create table requests(
        id bigint auto_increment,
        text varchar(256) not null
      )""".update.run
      _ <- sql"""create table results(
        id bigint,
        title varchar(128) not null,
        dataId varchar(128) not null,
        year varchar(4) not null,
        rating varchar(8) not null,
        nameEn varchar(256) not null
      )""".update.run
      _ <- sql"""create table links(
        id bigint,
        actor varchar(256) not null,
        trailers varchar(256) not null,
        frames varchar(256) not null,
        posters varchar(256) not null,
        seance varchar(256) not null,
        sites varchar(256) not null
      )""".update.run
    } yield ()

  def loadResults(text: String): ConnectionIO[Option[RawData]] =
    for {
      id <- sql"""select id from requests where text = $text""".query[Long].option
      rawData <- sql"""select title, dataId, year, rating, nameEn  from results where id = ${id.getOrElse(-1L)}""".query[(String, String, String, String, String)].option
      links <- sql"""select actor, trailers, frames, posters, seance, sites from links where id = ${id.getOrElse(-1L)}""".query[Links].option
    } yield rawData.map { case (title, dataId, year, rating, nameEn) => RawData(title, dataId, year, rating, nameEn, links.get)}

  def saveResults(text: String, rawData: RawData): ConnectionIO[RawData] =
    for {
      newId <- sql"""insert into requests(text) values ($text)"""
              .update
              .withUniqueGeneratedKeys[Long]("id")
       _ <- Update[(Long, String, String, String, String, String)]("insert into results(id, title, dataId, year, rating, nameEn) values (?, ?, ?, ?, ?, ?)")
         .run(newId, rawData.title, rawData.dataId, rawData.year, rawData.rating, rawData.nameEn)
      _ <- Update[(Long, String, String, String, String, String, String)]("insert into links(id, actor, trailers, frames, posters, seance, sites) values (?, ?, ?, ?, ?, ?, ?)")
         .run(newId, rawData.links.actor, rawData.links.trailers, rawData.links.frames, rawData.links.posters, rawData.links.seance, rawData.links.sites)

    } yield rawData
}

object Database extends Database
