package Parser

import java.net.URLEncoder
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.element
import scalaj.http.Http

object DownLoader {
  def downLoad(query: String):List[RawData] ={
    val text = "https://www.kinopoisk.ru/index.php?kp_query=" + URLEncoder.encode(query, "UTF-8")
    val request = Http(text).header("User-Agent", "Opera").asString
    val browser = JsoupBrowser()
    val doc = browser.parseString(request.body)
    val parser = new MainParser
    parser.getRawDatas(doc >> element("body"))
  }
}
