package Parser

import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.model.Element

case class RawData(title: String, dataId: String, year: String, rating: String, nameEn: String, links: Links) {

}

case class Links(actor: String, trailers: String, frames: String, posters: String, seance: String, sites: String) {

}

