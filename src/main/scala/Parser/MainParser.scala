package Parser

import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{element, elementList}

trait Parser {
  def getRawDatas(doc: Element): List[RawData]
}

case class MainParser() extends Parser {
  override def getRawDatas(doc: Element): List[RawData] = {
    val elementS = doc >> elementList(".element")
    elementS.filter(x => {
      val name = x >> element(".info .name .js-serp-metrika")
      (name.attrs.contains("data-type") && (name.attrs("data-type") == "film" || name.attrs("data-type") == "series"))
    }).map(getRawData)
  }

  def SetFromMainUrl(url: String): Links = {
    val linksActor = url + "/cast/#actor"
    val linksTrailer = url + "/video/"
    val linksFrames = url + "/stills/"
    val linksPoster = url + "/posters/"
    val linksSeance = url + "/afisha/"
    val linksSites = url + "/sites/"
    Links(linksActor, linksTrailer, linksFrames, linksPoster, linksSeance, linksSites)
  }

  private def getRawData(x: Element): RawData = {
    val mainURL = "https://www.kinopoisk.ru/film/"
    val name = x >> element(".info .name .js-serp-metrika")
    val filmURL = mainURL + name.attrs("data-id")
    val year = x >> element(".year")
    val ratingElement = x >?> element(".rating")
    val rating = ratingElement match {
      case None => null
      case Some(value) => value.attr("title")
    }
    val nameEn = x >> element(".gray")
    val links = SetFromMainUrl(mainURL + name.attrs("data-id"))

    RawData(name.innerHtml, filmURL, year.innerHtml, rating, nameEn.innerHtml, links)
  }
}