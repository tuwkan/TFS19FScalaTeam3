package bot.endpoint

final case class WebHookRequest(update_id: Long, message: Message)


final case class Message(date: Long, chat: FromParams,  message_id: Long, from: FromParams, text: String)


final case class FromParams(last_name: String, id: Long, first_name: String, username: String)