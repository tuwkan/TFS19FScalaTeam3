package bot.endpoint


import fintech.team3.AppService
import io.circe.Encoder
import io.circe.Decoder
import io.circe.generic.semiauto._
import monix.eval.Task
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import scala.util.Try

class WebHookEndpoint(dsl: Http4sDsl[Task], service: AppService) {
  import dsl._
  import WebHookEndpoint._

  implicit val decoder = jsonOf[Task, WebHookRequest]

  val routes = HttpRoutes.of[Task] {
    case req @ POST -> Root / "web_hook" =>
      for {
        request <- req.as[WebHookRequest]
        _ <- service.find(request.message.text, request.message.chat.id)
        response <- Ok()
      } yield response
  }
}

object LongExtractor {
  def unapply(str: String): Option[Long] = Try(str.toLong).toOption
}

object WebHookEndpoint {

  implicit val webHookRequestDecoder: Decoder[WebHookRequest] = deriveDecoder[WebHookRequest]
  implicit val messageDecoder: Decoder[Message] = deriveDecoder[Message]
  implicit val fromParamsDecoder: Decoder[FromParams] = deriveDecoder[FromParams]
}
